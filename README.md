# Cluster Management

This repository provides gitlab agents in our K8S clusters and also configures the "core services" for the cluster. Specifically, that includes:

- Certificate issuer
- Ingress
- Prometheus with blackbox exporter and alertmanager
- Grafana
- Librespeed speed test tool

## How do I add new things to the K8S cluster?

This repository should _only_ be used for services that are core to the cluster itself. Other applications should be put in their own repository and deployed from there using the GitLab agent tunnel pattern. See [dashy](https://gitlab.com/memhamwan/dashy) as an example of this in action.
